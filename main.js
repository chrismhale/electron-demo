'use strict';

const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const globalShortcut = electron.globalShortcut;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow, secondaryWindow;

var opts = {
  kiosk: true,
  width: 1200,
  height: 800,
  'web-preferences': {
    'node-integration': false
  }
}

function createWindow () {

  // Register a 'ctrl+x' shortcut listener.
  var kioskShortcut = globalShortcut.register('ctrl+k', function() {
    // Toggle kiosk mode
    if (mainWindow && mainWindow.isKiosk) {
      if (mainWindow.isKiosk()) {
        // Turn off kiosk
        mainWindow.setKiosk(false);
        // Set size
        mainWindow.setSize(1200, 800);
      } else {
        // Turn on Kiosk
        mainWindow.setKiosk(true);
      }
      // Reload
      mainWindow.reload();
    }
  });

  // Open/close dev tools on app window if open
  var devtoolsOpen = false;
  var devtoolsShortcut = globalShortcut.register('ctrl+d', function() {

    // Toggle
    if (mainWindow && mainWindow.openDevTools) {
      devtoolsOpen ? mainWindow.closeDevTools() : mainWindow.openDevTools({detach: false});
      // Switch state
      devtoolsOpen = !devtoolsOpen;
    }
  });


  // pop a window into the secondary display
  var popWindowShortcut = globalShortcut.register('ctrl+p', function() {

    var electronScreen = electron.screen;
    var primary = electronScreen.getPrimaryDisplay(),
        displays = electronScreen.getAllDisplays();

    //console.log(primary);
    //console.log(displays);
    var externalDisplay = null;
    for (var i in displays) {
      if (displays[i].bounds.x != 0 || displays[i].bounds.y != 0) {
        externalDisplay = displays[i];
        break;
      }
    }

    if (externalDisplay) {

      secondaryWindow = new BrowserWindow({
        kiosk: true,
        width: 1200,
        height: 800,
        'web-preferences': {
          'node-integration': false
        },
        x: externalDisplay.bounds.x,
        y: externalDisplay.bounds.y
      });

      secondaryWindow.loadURL('http://www.google.com');
    }
  });

  // Create the browser window.
  mainWindow = new BrowserWindow(opts);

  // and load the index.html of the app.
  mainWindow.loadURL('http://google.com');

  // Open the DevTools.
  //mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('will-quit', function() {
  // Unregister all shortcuts.
  globalShortcut.unregisterAll();

});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});
